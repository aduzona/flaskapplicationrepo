# AWS CI CD Pipeline

Build CI CD pipeline using AWS, Python Flask and Gitlab

```sh
python3 -m venv env
source env/bin/activate

pip install -r requirements.txt
```

check permissions
```sh
ls -l
```
* output: `-rw-r--r--  1 diego.uchendu  staff   36 30 Jan 16:16 build_docker.sh`
    * The above does not have permission to execute,thus no `x` in `-rw-r--r--`
* run `chmod +x build_docker.sh` to add permission to `build_docker.sh`


```sh
./build_docker.sh
```

* Go to dockerhub and create a repository called `flaskapp`
* Copy the name space `docker push aduzona/flaskapp:` from docker hub.
* login to docker `docker login --username=aduzona`
* tag our image to name space to my repository `docker tag flaskapp aduzona/flaskapp:latest`
* Push image to docker hub. `docker image push aduzona/flaskapp:latest`


# Build a CICD** 

Build a CICD around this Image in Dockerhub
* Create  Github Repo
* Ensure you configure git
  * `git config --global user.name "aduzona"`
  * `git config --global user.email "aduzona@gmail.com"`
  * `git config --list`

**Push Exiting folders:**

```sh
git init
git remote add origin https://gitlab.com/aduzona/flaskapplicationrepo.git 
git add .
git commit -m "Initial commit"
git remote add https://gitlab.com/aduzona/flaskapplicationrepo.git
git push -u origin master
```
# GitLab Runner

It is responsible for running script, the gitlab CI we are writting.
* Install GitLab runner, I used Homebrew installation
  ```sh
  brew install gitlab-runner
  brew services start gitlab-runner
  ```

* After Installation and stating runner, register our runner.
  ```sh
    sudo gitlab-runner register
  ```
  password: enter your system password.
* In your project, at the left side, Go to Settings->CI/CD -> Expand Runner -> Copy register with runner URL and paste it in terminal
  * Also copy registration token and enter in terminal.
  * Then write any description
  * Enter tags for the runner (comma-separated)(Note this is important): in this example we use `dockeryoutube`
  this tag mentioned will be attached to the `yaml` file, so when this project will be pushed to gitlab repository, the tags that you have mentioned in the yaml file, the same runner will be executed.
  This tag maps this project the to runner.
  * Enter optional maintenance note for the runner: `testing cicd`
  * Enter an executor: docker, ssh, virtualbox, instance, kubernetes, custom, docker-ssh, parallels, shell, docker+machine, docker-ssh+machine: `here I used shell` `shell` 
* verify it: `sudo gitlab-runner verify` Then runner is life.
* Now start the runner: `sudo gitlab-runner run`

## Create New Cluster in AWS ECS or EKS.

We will create a new cluster in AWS ECS
* 


# References

 1. [Install GitLab Runner Mac](https://docs.gitlab.com/runner/install/osx.html)
 2. [AWS CI CD Pipeline](https://www.youtube.com/watch?v=jDNf9p8uK9w&ab_channel=CloudGuru)